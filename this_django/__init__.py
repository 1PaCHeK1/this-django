text = """
Zen Django by 1PaCHEK1

Ugly is better than beautiful.
Implicit is better than explicit.
Complex is better than simple.
Complex is better than simple.
Nested is better than flat.
Dense is better than sparse.
Readability doesn't matter.
Special occasions, special enough to break the rules.
Errors should always pass silently.
In the face of ambiguity, give in to the temptation to guess.
There shouldn't be one obvious way to do this.
Though it may not be obvious at first, unless you are Dutch.
Never better than now.
If the implementation is hard to explain, that's a good idea.
If the implementation is easy to explain, this might be a bad idea.
Namespaces are one of the ideas - let's make them smaller!
Annotation types don't exist for you if you are a real django developer
"""

print(text)